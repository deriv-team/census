## Debian Derivatives census scripts

Welcome to the Debian Derivatives census scripts!

On a brief note, [Debian Derivatives Census](https://wiki.debian.org/Derivatives/Census) is an attempt to gather detailed information about Debian Derivatives that is useful to Debian, for integration of that information into Debian infrastructure and for the development of relationships between Debian and its derivatives.

**The Debian Derivatives census scripts** aim to take information added to the Debian Derivatives census wiki pages and aggregate, expand, check, massage and transform it in order to integrate information useful to Debian contributors into the Debian infrastructure.

The scripts are run by the make-based framework. For a detailed documentation, please refer to the `/doc` folder of this repository or you can also [click here](https://salsa.debian.org/deriv-team/census/tree/master/doc/README) to get started.
