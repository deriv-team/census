#!/usr/bin/python3

# Copyright 2018 Anastasiia Korobka
# Released under the MIT/Expat license, see doc/COPYING

# Find the reprepro.config file in derivatives' repositories
#
# Usage:
# get-reprepro-config <sources.list file>

import os
import sys
import logging
import requests
from debian.deb822 import Deb822

try:
	sources_list = sys.argv[1]
	sources_list_f = open(sources_list)
	sources_list = sources_list_f.readlines()
	sources_list_f.close()
except IOError:
	sys.exit()

links = set()
for line in sources_list:
	text = line
	line = line[:line.find('#')]
	line = line.strip()
	if not line: continue
	line = line.split(None, 1)
	line = line[1]
	if line[0] == '[':
		end_options = line.find(']')
	else:
		end_options = -1
	line = line[end_options+1:]
	line = line.split(None, 1)
	links.add(line[0])

retries = 3
timeout = 60
session = requests.Session()
adapter = requests.adapters.HTTPAdapter(max_retries=retries)
session.mount('http://', adapter)
session.mount('https://', adapter)
session.headers['User-Agent'] = 'Debian Derivatives Census QA bot'
debian_ssl_bundle = '/etc/ssl/ca-global/ca-certificates.crt'
if os.path.exists(debian_ssl_bundle):
	session.verify = debian_ssl_bundle

with open(sys.argv[2], 'wb') as target:
	for link in links:
		try:
			rep_request = session.get(os.path.join(link, 'conf/distributions'), timeout=timeout)
			if rep_request.status_code in [403, 404]:
				continue
			rep_request.raise_for_status()
			for par in Deb822.iter_paragraphs(sequence=rep_request.text):
				par['RepoUrl'] = link
				par.dump(target)
				target.write(b'\n')
		except requests.exceptions.RequestException as e:
			print(e)

if os.stat(sys.argv[2]).st_size == 0:
	os.remove(sys.argv[2])
