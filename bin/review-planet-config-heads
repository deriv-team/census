#!/bin/sh

# Copyright 2015, 2018 Paul Wise
# Released under the MIT/Expat license, see doc/COPYING

# Review the changes to Planet Derivatives config/heads.
#
# review-planet-config-heads

set -e
echo 'Downloading planet git (and making read-write), new config and new heads...'
get-planet-git planet-git
cd planet-git
if ! git config 'url.git@salsa.debian.org:.pushInsteadOf' > /dev/null ; then
	git config --local 'url.git@salsa.debian.org:.pushInsteadOf' 'https://salsa.debian.org/'
fi
cd ..
curl --config "$CURLRC" --silent --output planet-config http://deriv.debian.net/planet-config
find planet-git/heads/deriv -mindepth 1 -delete
curl --config "$CURLRC" --silent http://deriv.debian.net/planet-heads.tar |
tar --extract --directory planet-git/heads/deriv --file -
compare-planet-config planet-git planet-config | \
patch --no-backup-if-mismatch -p0 planet-git/config/config.ini.deriv
echo 'Comparing planet config and heads...'
cd planet-git
git diff config/config.ini.deriv
git add --intent-to-add heads/deriv/
git difftool --extcmd=compare-images heads/deriv/
if git diff config/config.ini.deriv | grep -q '^\+\[' ; then
	echo 'Please review the new RSS feeds:'
	git diff config/config.ini.deriv | grep '^\+\[' | sed 's_^\+\[__;s_]$__'
fi
echo 'Comparisons done, to commit the changes, please run these commands:'
echo 'cd planet-git && git commit -a && git push'
