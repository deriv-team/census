#!/usr/bin/python3

# Copyright 2011 Paul Wise
# Copyright 2018 Anastasiia Korobka
# Released under the MIT/Expat license, see doc/COPYING

# Checks the apt sources.list file for various issues
#
# Usage:
# check-sources-list <sources.list file> <reprepro.config file> <log file>

# FIXME: Split this into checking sources.list itself and against reprepro and Release files
# FIXME: update apt repository checks against the format documentation
#        https://wiki.debian.org/DebianRepository/Format

import os
import sys
import glob
import apt_pkg
import logging
from debian.deb822 import Deb822, Release
try:
	from urllib.parse import urlparse
except ImportError:
	from urlparse import urlparse

class sources_entry:
	def __repr__(self):
		return 'sources_entry(%r)' % self.__dict__

archs_file = os.path.realpath(os.path.join(__file__, '..', '..', 'etc', 'architectures'))
with open(archs_file) as f:
	archs = f.read().splitlines()

# Setup logging
try: os.remove(sys.argv[3])
except OSError: pass
logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG, filename=sys.argv[3])

# Read sources.list
try:
	sources_list = sys.argv[1]
	sources_list_f = open(sources_list)
	sources_list = sources_list_f.readlines()
	sources_list_f.close()
except IOError:
	logging.warning('no sources list')
	sys.exit()

# Parse sources.list
sources_entries = []
for line in sources_list:
	text = line
	line = line[:line.find('#')]
	line = line.strip()
	if not line: continue
	entry = sources_entry()
	entry.text = text.strip()
	line = line.split(None, 1)
	entry.type = line[0]
	line = line[1]
	if line[0] == '[':
		end_options = line.find(']')
		entry.options = line[1:end_options] if ']' in line else line[1:]
		entry.options = entry.options.split()
		entry.options = dict([opt.split('=',1) for opt in entry.options])
		entry.options = dict([(k, v.split(',') if k == 'arch' else v) for k, v in entry.options.items()])
	else:
		end_options = -1
		entry.options = {}
	line = line[end_options+1:]
	line = line.split(None, 2)
	entry.uri, entry.distribution = line[:2]
	entry.uri = urlparse(entry.uri)
	if len(line) > 2: entry.components = line[2].split()
	else: entry.components = []
	sources_entries.append(entry)

# Empty or fully commented sources.list
if not sources_entries:
	logging.warning('no sources entries')
	sys.exit()

if not [e for e in sources_entries if e.type == 'deb']:
	logging.warning('no deb entries')

if not [e for e in sources_entries if e.type == 'deb-src']:
	logging.warning('no deb-src entries')

def check_duplicate(option):
	duplicate_components = set(entry.components) & repos[entry.uri.geturl()][entry.distribution][entry.type][option]
	if duplicate_components:
		logging.warning('duplicate components found (%s): %s', ','.join(duplicate_components), entry.text)
	if len(entry.components) != len(set(entry.components)):
		logging.warning('duplicate components found: %s', entry.text)
	repos[entry.uri.geturl()][entry.distribution][entry.type][option].update(entry.components)

# Check sources.list
repos = {}
for entry in sources_entries:
	if entry.type not in ('deb','deb-src'):
		logging.warning('unknown repository type: %s', entry.text)
	if entry.type == 'deb' and not (entry.options and 'arch' in entry.options and entry.options['arch']):
		logging.warning('missing or empty arch= in options: %s', entry.text)
	if entry.type == 'deb-src' and 'arch' in entry.options:
		logging.warning('arch option in deb-src entry: %s', entry.text)
	if entry.type == 'deb' and 'arch' in entry.options and entry.options['arch'] and [arch for arch in entry.options['arch'] if arch not in archs]:
		# FIXME: highlight which architectures are new
		logging.info('new architecture(s): %s', entry.text)
	if entry.uri.scheme not in ('ftp', 'http', 'https'):
		logging.warning('unknown or inappropriate uri scheme: %s', entry.text)
	if '.debian.org' in entry.uri.hostname and '.alioth.debian.org' not in entry.uri.hostname:
		logging.warning('using a debian.org mirror: %s', entry.text)
	if '.debian-multimedia.org' in entry.uri.hostname or '.deb-multimedia.org' in entry.uri.hostname:
		logging.warning('using debian-multimedia.org, not needed: %s', entry.text)
	if not entry.distribution.endswith('/') and not entry.components:
		logging.warning('no components with distribution that is not an exact path: %s', entry.text)
	if entry.distribution.endswith('/') and entry.components:
		logging.warning('components with distribution that is an exact path: %s', entry.text)
	if [option for option in entry.options if option != 'arch']:
		logging.warning('unknown option: %s from %s', option, entry.text)
	if entry.uri.geturl() not in repos:
		repos[entry.uri.geturl()] = {}
	if entry.distribution not in repos[entry.uri.geturl()]:
		repos[entry.uri.geturl()][entry.distribution] = {}
	if entry.type not in repos[entry.uri.geturl()][entry.distribution]:
		repos[entry.uri.geturl()][entry.distribution][entry.type] = {}
	if entry.type == 'deb':
		for option in entry.options.get('arch', ['']):
			if option not in repos[entry.uri.geturl()][entry.distribution][entry.type]:
				repos[entry.uri.geturl()][entry.distribution][entry.type][option] = set()
			check_duplicate(option)
	else:
		repos[entry.uri.geturl()][entry.distribution][entry.type]['source'] = set()
		check_duplicate('source')

for uri, dists in repos.items():
	for dist, types in dists.items():
		if 'deb' not in types and 'deb-src' not in types:
			logging.warning('weird dist without deb/deb-src entries: %s %s', uri, dist)
		elif 'deb' not in types:
			logging.warning('deb-src without matching deb entry: %s %s %s', uri, dist, ','.join(types['deb-src']))
		elif 'deb-src' not in types:
			# FIXME: print the component name instead of the architecture list
			logging.warning('deb without matching deb-src entry: %s %s %s', uri, dist, ','.join(types['deb']))
		elif set.union(*types['deb'].values()) != set.union(*types['deb-src'].values()):
			# FIXME: ignore d-i subcomponents, their source is in the main component
			logging.warning('mismatched deb/deb-src entries: %s %s %s != %s', uri, dist,
				','.join(set.union(*types['deb'].values())), ','.join(set.union(*types['deb-src'].values())))

missing_elements = {}
missing_arches = {}
def check_arches(found_url, components, dist, arch_type, arch):
	if arch_type not in missing_arches:
		missing_arches[arch_type] = {}
	if dist not in missing_arches[arch_type]:
		missing_arches[arch_type][dist] = set()
	missing_comps = set()
	if arch not in repos[found_url][dist][arch_type]:
		missing_arches[arch_type][dist].add(arch)
		return
	else:
		missing_comps = set(filter(lambda comp: comp not in repos[found_url][dist][arch_type][arch],
				   components.split()))
	if arch_type not in missing_elements:
		missing_elements[arch_type] = {}
	if arch not in missing_elements[arch_type]:
		missing_elements[arch_type][arch] = {}
	missing_elements[arch_type][arch][dist] = missing_comps

def check_type(found_url, source_for_check, link):
	# FIXME: determine archive types from input data
	archive_type = ['deb']
	if 'source' in source_for_check['Architectures']:
		archive_type.append('deb-src')
		source_for_check['Architectures'] = source_for_check['Architectures'].replace('source', '')
	missing_types = {}
	for dist in names:
		if dist not in missing_types:
			missing_types[dist] = set()
		if dist not in repos[link]:
			continue
		for arch_type in archive_type:
			if arch_type not in repos[link][dist]:
				missing_types[dist].add(arch_type)
			elif arch_type == 'deb-src':
				check_arches(link, source_for_check['Components'], dist, arch_type, 'source')
			elif repos[link][dist][arch_type].keys() == ['']:
				check_arches(link, source_for_check['Components'],dist, arch_type, '')
			else:
				for option in source_for_check['Architectures'].split():
					check_arches(link, source_for_check['Components'],dist, arch_type, option)
	return missing_types

def print_warning(missing_types, pretty_name, found_url, type_of_file):
	missing_types = set.intersection(*missing_types.values())
	if missing_types:
		logging.warning('Archive type(s) %s of %s missing from the sources.list but present in the %s %s',
					','.join(missing_types), pretty_name, type_of_file, found_url)
	for arch_type in missing_arches:
		missing_arches[arch_type] = set.intersection(*missing_arches[arch_type].values())
		if missing_arches[arch_type]:
			logging.warning('Architecture(s) %s in %s missing from the sources.list but present in the %s %s',
						','.join(missing_arches[arch_type]), pretty_name, type_of_file, found_url)
	for arch_type in missing_elements:
		for arch in missing_elements[arch_type]:
			missing_elements[arch_type][arch] = set.intersection(*missing_elements[arch_type][arch].values())
			if missing_elements[arch_type][arch]:
				logging.warning('Component(s) %s of architecture %s in %s missing from the sources.list but present in the %s %s',
							','.join(missing_elements[arch_type][arch]), arch, pretty_name, type_of_file, found_url)
	missing_arches.clear()
	missing_types.clear()
	missing_elements.clear()

def preparation_for_checking(source_for_comparing):
	distribution = ['Suite', 'Codename']
	distribution = filter(lambda x: x in source_for_comparing, distribution)
	names = list(map(lambda x: source_for_comparing[x], distribution))
	pretty_name = names[0] if len(names) < 2 else '{} ({})'.format(names[0], names[1])
	return names, pretty_name

# Check sources.list against reprepro config file
try:
	reprepro_file = open(sys.argv[2],'r')
	for derivative in Deb822.iter_paragraphs(sequence=reprepro_file):
		names, pretty_name = preparation_for_checking(derivative)
		conf_url = os.path.join(derivative['RepoUrl'], 'conf/distributions')
		if not any(name in repos[derivative['RepoUrl']] for name in names):
			# FIXME: add a mechanism to ignore Ubuntu related suites
			logging.warning('Distribution %s is missing from the sources.list but is present in the reprepro config %s',
				pretty_name, conf_url)
			continue
		miss_types = check_type(derivative['RepoUrl'], derivative, derivative['RepoUrl'])
		print_warning(miss_types, pretty_name, conf_url, 'reprepro config')
except:
	pass

#Check sources.list against Architectures/Components in Release files
apt_pkg.init()
acquire = apt_pkg.Acquire()
sources_list = apt_pkg.SourceList()
sources_list.read_main_list()
sources_list.get_indexes(acquire, True)

links = {}
for link in sources_entries:
	if link.uri.geturl().startswith('https://') and link.uri.geturl() not in links:
		links[link.uri.geturl()[8:]] = link.uri.geturl()
	elif link.uri.geturl().startswith('http://') and link.uri.geturl() not in links:
		links[link.uri.geturl()[7:]] = link.uri.geturl()
for item in acquire.items:
	if 'Release' in item.desc_uri and 'Release.gpg' not in item.desc_uri:
		fn = item.destfile + '.reverify'
		if not os.path.exists(fn):
			fn = fn.replace('.reverify','')
		if not os.path.exists(fn):
			fn = fn.replace('/partial/','/')
		if not os.path.exists(fn):
			fn = fn.replace('InRelease','Release')
		file_ = fn.replace('_', '/')
		found_url = set(filter(lambda entry: file_.find(entry), links))
		if not found_url:
			logging.warning('URL from file %s is missing from the sources.list', file)
		else:
			found_distribution = []
			for f_url in found_url:
				temp_dist = filter(lambda dist: file_.find(dist), repos[links[f_url]])
			if temp_dist:
				found_distribution.append(temp_dist)
				found_url = f_url
		found_distribution = list(filter(lambda x: x, found_distribution))
		if not found_distribution:
			logging.warning('Distribution from file %s is missing from the sources.list', file)
			continue
		try: f = open(fn)
		except IOError:
			continue
		rel = Release(f)
		try:
			# FIXME: check sources.list is using Suite rather than Codename
			# FIXME: check for mismatch between Architectures and binary-*/ files
			names, pretty_name = preparation_for_checking(rel)
			miss_types = check_type(found_url, rel, links[found_url])
			# FIXME: change the filename to a URL
			print_warning(miss_types, pretty_name, fn, 'release file')
		except:
			pass
