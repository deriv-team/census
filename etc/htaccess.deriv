IndexIgnore Makefile HEADER.html FOOTER.html style.css
IndexOptions -FoldersFirst

# Configuration files
AddDescription "A list of disabled Makefile targets"                              disabled-targets

# Timestamps
AddDescription "Remote timestamp to reduce downloading"                         *.remote-timestamp
AddDescription "Local timestamp"                                                *-local-timestamp

# Debian sources.list file
AddDescription "Debian sources.list snippets"                                     debian.sources.list

# Contents of the apt/ and debian.apt/ directories
AddDescription " "                                                                apt/

# Information extracted from the wiki and downloaded from the Internet
AddDescription "Long name"                                                        name
AddDescription "Derivative's status (active/inactive)"                            status
AddDescription "URL to the homepage"                                              homepage
AddDescription "The logo image"                                                   logo-img
AddDescription "URL to the logo"                                                  logo
AddDescription "URLs to RSS feeds for the developer blogs"                        blogs-dev-rss
AddDescription "URLs to RSS feeds for the main blogs"                             blogs-rss
AddDescription "URLs to the developer blogs"                                      blogs-dev
AddDescription "URLs to the main blogs"                                           blogs
AddDescription "The identifier from distrowatch.com"                              distrowatch
AddDescription "The sources.list snippets"                                        sources.list
AddDescription "Debian apt package data, including Release/Package/Sources files" debian.apt
AddDescription "apt package data, including Release/Package/Sources files"        apt
AddDescription "The text version of the wiki pages"                               wiki.txt
AddDescription "dpkg vendor"                                                      vendor
AddDescription "Email addresses subscribed to the derivative's error output"      subscribers
AddDescription "The derivative's reprepro.config"                                 reprepro.config

# dd-lists
AddDescription "For those packages the Sources files are missing"                 sources.patches.dd-list.unknown-packages
AddDescription "Maintainer/Uploaders data for the packages"                       sources.patches.dd-list

# Files created on snapshot.debian.org
AddDescription "URLs to modified source packages"                                 sources.links
AddDescription "Source packages that were never in Debian"                        sources.new
AddDescription "Package name/version and patches"                                 sources.patches
AddDescription "Implementation detail due to a deficiency in make"                sources.stamp
AddDescription "YAML mapping between SHA-1 and other hashes of modified files" 	  sources.files
AddDescription "Symlinks mapping human-readable patch names to SHA-1 based names" patches
AddDescription "Errors and warnings from the log file"                            sources.log.warnings
AddDescription "Log file from the compare-source-package-list script"             sources.log
AddDescription "error.log to be sent to the subscribers"                          error.log.to-send
AddDescription "Summary of the derivative's error output"                         error.log

# Problem reports
AddDescription "Potential issues with the sources.list snippet"                   check-sources-list
AddDescription "Potential issues with the apt metadata"                           check-package-list
AddDescription "List of binary packages in Packages files"                        packages_binary_packages
AddDescription "List of binary packages in Sources files"                         sources_binary_packages
AddDescription "List of source packages in Packages files"                        packages_source_packages
AddDescription "List of source packages in Sources files"                         sources_source_packages
AddDescription "diff of sources_source_packages and packages_source_packages"     diff_source_packages
AddDescription "diff of sources_binary_packages and packages_binary_packages"     diff_binary_packages

# Planet Debian
AddDescription "Small logo for Planet Debian Derivatives"                         planet-head
AddDescription "Config file for Planet Debian Derivatives"                        planet-config
